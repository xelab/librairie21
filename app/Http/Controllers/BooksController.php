<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Book;
use App\Distributor;
use App\Publisher;
use App\Author;
use App\Tag;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Validator;
use Log;
use Goutte\Client;
use App\DataTables\BooksDataTable;

class BooksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(BooksDataTable $dataTable)
    {
        $distributors = ['' => 'Choisir un distributeur'] + Distributor::pluck('name', 'id')->all();
        $publishers = ['' => 'Choisir un éditeur'] + Publisher::pluck('name', 'id')->all();
        $authors = Author::pluck('name', 'id')->all();
        $tags = Tag::pluck('name', 'id')->all();
        $collections = array();
        $book = new Book;
        return $dataTable->render('books.index', compact('distributors', 'publishers', 'authors', 'tags', 'collections', 'book'));
    }

    /**
     * Process datatables ajax/actions requests.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData(BooksDataTable $dataTable)
    {
        return $dataTable->render(null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'released' => 'date_format:"d/m/Y"'
        ]);

        if($validator->fails()){
            $this->throwValidationException(
                $request, $validator
            );
        }

        $data = $this->changeFields($data);

        $book = Book::create($data);

        if(isset($data['authors']) && !empty($data['authors']))
        {
            $book->authors()->attach($data['authors']);
        }

        if(isset($data['tags']) && !empty($data['tags']))
        {
            $book->tags()->attach($data['tags']);
        }

        if($request->wantsJson())
        {
            return response()->json($book);
        }

        return redirect('book');
    }

    function changeFields($data)
    {
        if(isset($data['released']) && !empty($data['released']))
        {
            $date = Carbon::createFromFormat('d/m/Y', $data['released']);
            $data['released'] = $date->format('Y-m-d');
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $book = Book::findOrFail($id);

        if($request->wantsJson())
        {
            return response()->json($book);
        }

        return view('books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $distributors = ['' => 'Choisir un distributeur'] + Distributor::pluck('name', 'id')->all();
        $publishers = ['' => 'Choisir un éditeur'] + Publisher::pluck('name', 'id')->all();
        $authors = Author::pluck('name', 'id')->all();
        $tags = Tag::pluck('name', 'id')->all();
        $collections = array();
        if($book->released) $book->released = Carbon::parse($book->released)->format('d/m/Y');
        if($book->publisher_id && count($book->publisher->collections) > 0)
        {
            $collections = $book->publisher->collections->pluck('name', 'id')->all();
        }
        return view('books.edit', compact('book', 'distributors', 'publishers', 'authors', 'tags', 'collections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        if(!$request->wantsJson())
        {
            $this->validate($request, ['title' => 'required', 'released' => 'date_format:"d/m/Y"']);
        }
        
        $data = $this->changeFields($request->all());

        $book = Book::findOrFail($id);
        $book->update($data);

        if(isset($data['authors']) && !empty($data['authors']))
        {
            $book->authors()->sync($data['authors']);
        }

        if(isset($data['tags']) && !empty($data['tags']))
        {
            $book->tags()->sync($data['tags']);
        }

        if($request->wantsJson())
        {
            return response()->json($book);
        }
        return redirect('book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Book::destroy($id);
        return redirect('book');
    }

    public function scrapCsv()
    {
        $csvFile = file_get_contents(storage_path('csv/output.csv'));
        $csvLines = explode("\n", $csvFile);
        foreach ($csvLines as $line) {
            $items = explode("\t", $line);
            $book = null;
            if(count($items) > 0)
            {
                if(!empty($items[1]))
                {
                    $data = $this->scraping(new Request, $items[1]);
                    if($data['book'] != null)
                    {
                        $book = $data['book'];

                        $book->isbn = $items[1];
                        $book->summary = $items[8];
                        if($book->released) $book->released = Carbon::createFromFormat('d/m/Y', $book->released)->format('Y-m-d');

                        $book->save();

                        if(isset($data['authors']) && !empty($data['authors']))
                        {
                            $data['authors'] = array_merge($data['authors']->all(), $data['newAuthors']);
                        }
                        else
                        {
                            $data['authors'] = $data['newAuthors'];
                        }

                        if(isset($data['authors']) && !empty($data['authors']))
                        {
                            $book->authors()->attach(array_pluck($data['authors'], 'id'));
                        }

                        if(!empty($items[0]))
                        {
                            $tags = Tag::where('name', $items[0])->get();
                            if(count($tags) == 0)
                            {
                                $tag = Tag::create(['name' => $items[0]]);
                                $data['newTags'][] = $tag;
                            }
                            else
                            {
                                $data['newTags'][] = $tags[0];
                            }
                        }
                        
                        if(isset($data['tags']) && !empty($data['tags']))
                        {
                            $data['tags'] = array_merge($data['tags']->all(), $data['newTags']);
                        }
                        else
                        {
                            $data['tags'] = $data['newTags'];
                        }

                        if(isset($data['tags']) && !empty($data['tags']))
                        {
                            $book->tags()->attach(array_pluck($data['tags'], 'id'));
                        }
                    }
                }
                if(!isset($book))
                {
                    $book = new Book;
                    $book->isbn = $items[1];
                    $book->title = $items[2];
                    $book->summary = $items[8];
                    if(!empty($items[7]))
                    {
                        $price = str_replace(',', '.', $items[7]);
                        if(is_numeric($price))
                        {
                            $book->price = $price;
                        }
                        
                    }
                    if(!empty($items[4]))
                    {
                        $editeur = Publisher::where(['name' => $items[4]])->first();
                        if ($editeur != null)
                        {
                            $book->publisher_id = $editeur->id;
                        }
                        else
                        {
                            $newPublisher = Publisher::create(['name' => $items[4]]);
                            $book->publisher_id = $newPublisher->id;
                        }
                    }
                    
                    $book->save();

                    if(!empty($items[3]))
                    {
                        $authors = Author::where('name', $items[3])->get();
                        if(count($authors) == 0)
                        {
                            $author = Author::create(['name' => $items[3]]);
                            $book->authors()->attach($author->id);
                        }
                        elseif (count($authors) >= 1)
                        {
                            $book->authors()->attach($authors[0]->id);
                        }
                    }

                    if(!empty($items[0]))
                    {
                        $tags = Tag::where('name', $items[0])->get();
                        if(count($tags) == 0)
                        {
                            $tag = Tag::create(['name' => $items[0]]);
                            $book->tags()->attach($tag->id);
                        }
                        else
                        {
                            $book->tags()->attach($tags[0]->id);
                        }
                    }
                }
            }
             
        }
        dd('Terminé');
        
    }

    public function scraping(Request $request, $isbn = null)
    {
        $client = new Client();
        if($isbn)
        {
            $crawler = $client->request('GET', 'http://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=simple&LIVREANCIEN=2&MOTS='. $isbn .'&x=0&y=0');
        }
        else
        {
            $crawler = $client->request('GET', 'http://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=simple&LIVREANCIEN=2&MOTS='. $request->input('isbn') .'&x=0&y=0');
        }
        $count = $crawler->filter('.listeliv_metabook')->count();
        $book = null;
        $authors = array();
        $newAuthors = array();
        $tags = array();
        $newTags = array();
        $newPublisher = null;
        if($count > 0)
        {
            $book = new Book;
            if(count($element = $crawler->filter('.listeliv_metabook > .titre_commentaire > .titre a')->first()) > 0){
                $book->title = $element->text();
            }
            if(count($element = $crawler->filter('.listeliv_metabook > .auteurs > a')->first()) > 0)
            {
                $names = array_map('trim', explode(',', $element->text()));
                $authors = Author::whereIn('name', $names)->get();
                if(count($authors) != count($names))
                {
                    $namesInBdd = $authors->pluck('name')->all();
                    foreach ($names as $name) {
                        if(!in_array($name, $namesInBdd))
                        {
                            $author = Author::create(['name' => $name]);
                            $newAuthors[] = $author;
                        }
                    }
                }

            }
            if(count($element = $crawler->filter('.listeliv_metabook > .editeur')->first()) > 0)
            {
                if (count($crawler->filter('.listeliv_metabook > .editeur .date_parution')->first()) > 0) 
                {
                    $date = $crawler->filter('.listeliv_metabook > .editeur .date_parution')->first()->text();
                    $english = array('Jan','Febr','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec');
                    $french = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');
                    $date = str_replace($french, $english, $date);
                    $date = new Carbon(trim(str_replace($french, $english, $date)));
                    $book->released = $date->format('d/m/Y');
                }
                
                $editeur = explode('-', $element->text());
                $nom = trim(html_entity_decode($editeur[0]));
                $editeur = Publisher::where(['name' => $nom])->first();
                if ($editeur != null)
                {
                    $book->publisher_id = $editeur->id;
                }
                else
                {
                    $newPublisher = Publisher::create(['name' => $nom]);
                    $book->publisher_id = $newPublisher->id;
                }
            }
            if(count($element = $crawler->filter('.listeliv_metabook > .prix > .prix_indicatif')->first()) > 0)
            {
                preg_match('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', $element->text(), $matches);
                if(count($matches) >0)
                {
                    $book->price = $matches[0];
                }
            }
            if(count($element = $crawler->filter('.listeliv_metabook > .genre')->first()) > 0)
            {
                $names = explode(',', $element->text());
                $tags = Tag::whereIn('name', $names)->get();
                if(count($tags) != count($names))
                {
                    $namesInBdd = $tags->pluck('name')->all();
                    foreach ($names as $name) {
                        if(!in_array($name, $namesInBdd) && !empty($name))
                        {
                            $tag = Tag::create(['name' => $name]);
                            $newTags[] = $tag;
                        }
                    }
                }
            }
            if (count($element = $crawler->filter('.visu > a > img')->first()) > 0)
            {
                $book->url_picture = $element->attr('src');
            }
        }
        if($isbn)
        {
            return ['book' => $book, 'tags' => $tags, 'newTags' => $newTags, 'authors' => $authors, 'newAuthors' => $newAuthors, 'newPublisher' => $newPublisher];
        }
        else
        {
            return response()->json(['book' => $book, 'tags' => $tags, 'newTags' => $newTags, 'authors' => $authors, 'newAuthors' => $newAuthors, 'newPublisher' => $newPublisher]);
        }
        
    }
}
