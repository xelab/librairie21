@extends('layouts.master')

@section('content')
    <button onclick="$('#newBook').show();$(this).hide();$('#hideForm').show();" id="showNewForm" class="btn btn-primary">Ajouter une entrée</button>
    <button onclick="$('#newBook').hide();$('#showNewForm').show();$(this).hide();" class="btn btn-warning" id="hideForm" style="display: none">Masquer le formulaire</button>
    
    <div class="alert alert-success alert-dismissible hidden" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Ouvrage correctement ajouté.</strong>
    </div> 

    @include('books._form')
    @include('books._tags')
    @include('books._distributors')
    @include('books._publishers')
    @include('books._collections')
    @include('books._authors')

    <div class="row">
        <div class="col-xs-12">
            <hr>
            {!! $dataTable->table(['id' => 'books-table', 'class' => 'table table-striped']) !!}
        </div>
    </div>
    
    <div id="pop-book-form" class="white-popup mfp-hide">
        <div class="panel panel-default">
            <div class="panel-heading">Modification rapide</div>
            <div class="panel-body">
                {!! Form::open(['route' => ['book.update', 0], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'editBook']) !!}
                    <div class="col-xs-12">
                        <div class="form-group">
                            {!! Form::label('buy', 'Quantité achats fermes: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('buy', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('deposit', 'Quantité dépôts: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('deposit', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <a href="{{ route('book.edit', ['id' => 0]) }}">Modifications avancées</a>
                            </div>    
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary form-control']) !!}
                            </div>    
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('js/jquery.form.min.js') }}"></script>
    <script src="{{ asset('js/books.js') }}"></script>
    {!! $dataTable->scripts() !!}
    <script>
        $(function() {
            
            $('#newBook').ajaxForm({success: addBook, error: showErrors, dataType: 'json', resetForm: true});
            $('#newPublisher').ajaxForm({success: addAndHidePublisherForm, dataType: 'json', resetForm: true});
            $('#newCollection').ajaxForm({success: addAndHideCollectionForm, dataType: 'json', resetForm: true});
            $('#newDistributor').ajaxForm({success: addAndHideDistributorForm, dataType: 'json', resetForm: true});
            $('#newAuthor').ajaxForm({success: addAndHideAuthorForm, dataType: 'json', resetForm: true});
            $('#newTag').ajaxForm({success: addAndHideTagForm, dataType: 'json', resetForm: true});
            $('#editBook').ajaxForm({success: hideBookForm, dataType: 'json', resetForm: true});
        });
    </script>
@endpush