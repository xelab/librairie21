@extends('layouts.master')

@section('content')

    <h1>Modifier l'ouvrage</h1>
    <hr/>

    @include('books._form')

    @include('books._tags')
    @include('books._distributors')
    @include('books._publishers')
    @include('books._collections')
    @include('books._authors')

    {!! Form::model($book, ['route' => ['book.destroy', $book->id], 
                            'method' => 'delete', 'id' => 'destroyBook',
                            'onsubmit' => "return confirm('Êtes-vous sûr de vouloir supprimer?')",
                            'class' => "pull-right"]) !!}
        <input type="submit" class="btn btn-danger" value="Supprimer l'ouvrage"/>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection

@push('scripts')
    <script src="{{ asset('js/jquery.form.min.js') }}"></script>
    <script src="{{ asset('js/books.js') }}"></script>
    <script>
        $(function() {
            $('#newPublisher').ajaxForm({success: addAndHidePublisherForm, dataType: 'json', resetForm: true});
            $('#newCollection').ajaxForm({success: addAndHideCollectionForm, dataType: 'json', resetForm: true});
            $('#newDistributor').ajaxForm({success: addAndHideDistributorForm, dataType: 'json', resetForm: true});
            $('#newAuthor').ajaxForm({success: addAndHideAuthorForm, dataType: 'json', resetForm: true});
            $('#newTag').ajaxForm({success: addAndHideTagForm, dataType: 'json', resetForm: true});
        });
    </script>
@endpush