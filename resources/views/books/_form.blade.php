<input type="hidden" id="url-scraping" value="{{ route('books.scraping') }}">
<input type="hidden" id="url-collections" value="{{ route('publisher.collections') }}">
<input type="hidden" id="book-show" value="{{ route('book.show', ['id' => 0]) }}">

@if(!isset($book->id))
    {!! Form::model($book, ['route' => 'book.store', 'class' => 'form-horizontal', 'id' => 'newBook']) !!}
@else
    {!! Form::model($book, ['route' => ['book.update', $book->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'editBook']) !!}
@endif

    <hr>
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('isbn', 'ISBN: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::number('isbn', $book->isbn, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('title', 'Titre: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('title', $book->title, ['class' => 'form-control']) !!}
                <small class="help-block"></small>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('publisher_id', 'Éditeur : ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    {!! Form::select('publisher_id', $publishers, $book->publisher_id, ['class' => 'form-control']) !!}
                    <a class="input-group-addon new-publisher open-popup-link" href="#pop-publisher-form">+</a>
                </div>
            </div>
        </div>
        <div class="form-group collection" style="@if($book->publisher_id == null) display:none @endif">
            {!! Form::label('collection_id', 'Collection: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    {!! Form::select('collection_id', $collections, $book->collection_id, ['class' => 'form-control']) !!}
                    <a class="input-group-addon new-collection open-popup-link" href="#pop-collection-form">+</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('authors', 'Auteurs: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    {!! Form::select('authors', $authors, $book->authors->pluck('id')->all(), ['class' => 'form-control', 'multiple' => 'multiple', 'name'=>'authors[]']) !!}
                    <a class="input-group-addon new-author open-popup-link" href="#pop-author-form">+</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Prix: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::number('price', $book->price, ['class' => 'form-control', 'step' => 'any', 'min' => '0']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('released', 'Publié le: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::date('released', $book->released, ['class' => 'form-control']) !!}
                <small class="help-block"></small>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('deposit', 'Dépôts: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::number('deposit', $book->deposit, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('buy', 'Achats fermes: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::number('buy', $book->buy, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('distributor_id', 'Distributeur: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    {!! Form::select('distributor_id', $distributors, $book->distributors, ['class' => 'form-control']) !!}
                    <a class="input-group-addon new-distributor open-popup-link" href="#pop-distributor-form">+</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('tags', 'Tags: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    {!! Form::select('tags', $tags, $book->tags->pluck('id')->all(), ['class' => 'form-control', 'multiple' => 'multiple', 'name'=>'tags[]']) !!}
                    <a class="input-group-addon new-tag open-popup-link" href="#pop-tag-form">+</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('url_picture', 'URL image: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('url_picture', $book->url_picture, ['class' => 'form-control']) !!}
                <img id="picture" src="{{ $book->url_picture or '#' }}" alt="">
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('summary', 'Résumé: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::textarea('summary', $book->summary, ['class' => 'form-control', 'rows' => 5]) !!}
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                @if(!isset($book->id))
                    {!! Form::submit('Créer', ['class' => 'btn btn-primary form-control']) !!}
                @else
                    {!! Form::submit('Appliquer les modifications', ['class' => 'btn btn-primary form-control']) !!}
                @endif
            </div>    
        </div>
    </div>
{!! Form::close() !!}